# Plantilla de _superproyecto_

Este _superproyecto_ servirá para agrupar los proyectos comunes a una aplicación mayor.

Pasos a seguir para inicializar el _superproyecto_:

* Clonar el repositorio: `git clone https://gitlab.com/xnacho/<repo>.git`

* Inicializar los submódulos:
>```bash
cd <repo>
git submodule update --init
```
_**Nota:** para mayor practicidad, se incluye un ejecutable "iniciar-superproyecto.bat", que inicializa los submódulos del repositorio contenedor._

